import React from 'react';

function Main({ children }) {
  return (
    <main className="p-4">
      {children}
    </main>
  );
}

export default Main;
