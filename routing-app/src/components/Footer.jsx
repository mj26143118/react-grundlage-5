import React from 'react';

function Footer() {
  return (
    <footer className="bg-blue-500 text-white p-4 text-center">
      © 2024 React Grundlage Website. All rights reserved.
    </footer>
  );
}

export default Footer;
