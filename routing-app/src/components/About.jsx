import React from 'react';

function About() {
  return (
    <div>
      <h2>About Us</h2>
      <p>This is the about page. Here's some information about us.</p>
    </div>
  );
}

export default About;
