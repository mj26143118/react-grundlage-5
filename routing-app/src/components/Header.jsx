import React from 'react';
import { NavLink } from 'react-router-dom';

function Header() {
  return (
    <header className="bg-blue-500 text-white p-4 flex justify-between items-center shadow-md">
      <h1 className="text-lg font-bold">React Grundlage Website</h1>
      <nav>
        <NavLink to="/" className={({ isActive }) => isActive ? "text-red-500 px-4" : "text-white px-4"}>Home</NavLink>
        <NavLink to="/about" className={({ isActive }) => isActive ? "text-red-500 px-4" : "text-white px-4"}>About</NavLink>
        <NavLink to="/contact" className={({ isActive }) => isActive ? "text-red-500 px-4" : "text-white px-4"}>Contact</NavLink>
        <NavLink to="/login" className={({ isActive }) => isActive ? "text-red-500 px-4" : "text-white px-4"}>Login</NavLink>
      </nav>
    </header>
  );
}

export default Header;
